import React, { useEffect, useState } from "react";
import {
    View,
    Text,
    StyleSheet,
    Button,
    AppState,
    Platform,
    Alert
} from "react-native";

import FingerprintScanner from 'react-native-fingerprint-scanner';

import CustomModal from './CustomModal'


const LoginScreen = (props) => {

    const [loginState, setLoginState] = useState('Not Logged In')
    const [bioType, setBioType] = useState('')
    const [appState, setAppState] = useState(AppState.currentState);
    const [modalVisible, setModalVisible] = useState(false);

    useEffect(() => {
        AppState.addEventListener('change', handleAppStateChange);
        return (() => {
            AppState.removeEventListener('change', handleAppStateChange);
        })
    }, []);

    const handleAppStateChange = (state) => {
        setAppState(state);
    }

    useEffect(() => {
        console.log(appState)
        FingerprintScanner
            .isSensorAvailable()
            .then(biometryType => {
                setBioType(biometryType)
                console.log(biometryType)
            })
            .catch(error => {
                setBioType(error.message)
                console.log(error.message)
            })
    });

    const handleAuthenticationAttemptedLegacy = (error) => {
        alert(error)
    };


    const requiresLegacyAuthentication = () => {
        return Platform.Version < 23;
    }

    const authCurrent = () => {
        FingerprintScanner
            .authenticate({ title: 'Log in', subTitle: 'Some Subtitle', description: 'Some description', cancelButton: 'some cancel btn', onAttempt: handleAuthenticationAttemptedLegacy })
            .then(() => {
                console.log('loged in')
                setLoginState('You are Logged In')
            })
            .catch((e) => {
                alert(e.message)
            })
    }

    const authLegacy = () => {
        FingerprintScanner
            .authenticate({ onAttempt: handleAuthenticationAttemptedLegacy })
            .then(() => {
                Alert.alert('Fingerprint Authentication', 'Authenticated successfully')
                setLoginState('You are Logged In')
            })
            .catch((error) => {
                alert(error.message)
                console.log(error)
            })
    }

    const loginBio = () => {
        if (requiresLegacyAuthentication()) {
            authLegacy();
        } else {
            authCurrent();
        }
    }

    const logoutBio = () => {
        FingerprintScanner.release()
        setLoginState('Not Logged In')
    }

    const loginToggle = () => {
        if (loginState == 'Not Logged In') {
            return (
                <Button title='Login BIO' onPress={() => loginBio()} />
            )
        } else {
            return (
                <Button title='Logout BIO' onPress={() => logoutBio()} />
            )
        }
    }

    const updateVisibilityModalAlert = (visibility) => {
        setModalVisible(visibility)
    }

    return (
        <View style={styles.container}>

            <Text style={{ margin: 20, fontSize: 18 }}>Bio type: {bioType}</Text>
            <Text style={{ margin: 20, fontSize: 18  }}>{loginState}</Text>


            {loginToggle()}

            <View style={{ margin: 20 }}>
                <Button title='MODAL' onPress={() => setModalVisible(true)} />
            </View>
            <CustomModal isVisibleModal={modalVisible} updateVisibilityModalAlert={updateVisibilityModalAlert} />

        </View>
    )
}

export default LoginScreen;

const styles = StyleSheet.create({
    container: {
        flex: 1,
        alignItems: 'center',
    }
});