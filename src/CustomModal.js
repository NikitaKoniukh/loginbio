import React from "react";
import {
    Modal,
    StyleSheet,
    Text,
    View,
    Image,
    TouchableOpacity,
    Dimensions,
} from "react-native";
import FingerprintScanner from 'react-native-fingerprint-scanner';

const CustomModal = (props) => {




    const bio = () => {
        if (props.isVisibleModal === true) {
            FingerprintScanner
                .authenticate({
                    description: 'Log in with Biometrics',
                    onAttempt: handleAuthenticationAttempted(),
                })
                .then(() => {
                    alert('Fingerprint Authentication', 'Authenticated successfully');
                })
                .catch((error) => {
                    alert(error.message);
                });
        }
    }

    const handleAuthenticationAttempted = (error) => {
        alert(error)
    }

    return (
        <View style={styles.centeredView}>
            <Modal
                animationType='fade'
                transparent={true}
                visible={props.isVisibleModal}>
                <View style={styles.centeredView}>
                    <View style={styles.modalView}>
                        {/* {bio()} */}
                        <Image style={styles.alertImage} source={require('./assets/finger_print.png')} />
                        <Text style={styles.titleAlert}>title</Text>
                        <Text style={styles.subTitleAlert}>message</Text>

                        <TouchableOpacity onPress={() => { props.updateVisibilityModalAlert(false) }}>
                            <View style={[styles.buttonOK, { marginTop: 44 }]}>
                                <Text style={styles.buttonTextYes}>cancel</Text>
                            </View>
                        </TouchableOpacity>

                    </View>
                </View>
            </Modal>
        </View>
    );
};

const styles = StyleSheet.create({
    centeredView: {
        flex: 1,
        justifyContent: "center",
        alignItems: "center",
        backgroundColor: 'rgba(48, 48, 48, 0.8)'
    },
    modalView: {
        margin: 20,
        height: 343,
        width: 300,
        backgroundColor: "white",
        borderRadius: 20,
        alignItems: "center",
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 2
        },
        shadowOpacity: 0.25,
        shadowRadius: 3.84,
        elevation: 5
    },
    openButton: {
        backgroundColor: "#F194FF",
        borderRadius: 20,
        padding: 10,
        elevation: 2
    },
    alertImage: {
        width: 59.9,
        height: 53.5,
        marginTop: 49.3
    },
    titleAlert: {
        fontSize: 30,
        marginTop: 30.3,
    },
    subTitleAlert: {
        textAlign: 'center',
        fontSize: 18,
        marginTop: 12,
    },
    buttonOK: {
        height: 40,
        justifyContent: 'center',
        alignItems: 'center',
        width: Dimensions.get('window').width - (Dimensions.get('window').width - 300) - 106,
        borderRadius: 30,
        shadowOffset: { width: 0, height: 3 },
        shadowOpacity: 0.2,
        shadowRadius: 3,
        elevation: 3,
    },
    buttonTextYes: {
        textTransform: 'uppercase',
        textAlign: 'center',
        fontSize: 16,
    },
    buttonTextNo: {
        textTransform: 'uppercase',
        textAlign: 'center',
        fontSize: 16,
    }
});

export default CustomModal;
