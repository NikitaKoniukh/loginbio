import React, { useEffect } from "react";
import {
    View,
    Text,
    StyleSheet,
    Image,
    TouchableOpacity,
    Platform
} from "react-native";

import FingerprintScanner from 'react-native-fingerprint-scanner';

const NewLoginScreen = (props) => {

    useEffect(() => {
        if (requiresLegacyAuthentication()) {
            authLegacy();
        } else {
            authCurrent();
        }
    }, [])


    const requiresLegacyAuthentication = () => {
        return Platform.Version < 23;
    }

    const authCurrent = () => {
        FingerprintScanner
            .authenticate({ description: 'lkjfhl' || 'Log in with Biometrics' })
            .then(() => {
                // this.props.onAuthenticate();
            });
    }

    const authLegacy = () => {
        FingerprintScanner
            .authenticate({ onAttempt: handleAuthenticationAttemptedLegacy() })
            .then(() => {
                // this.props.handlePopupDismissedLegacy();
                // Alert.alert('Fingerprint Authentication', 'Authenticated successfully');
            })
            .catch((error) => {
                // this.setState({ errorMessageLegacy: error.message, biometricLegacy: error.biometric });
                // this.description.shake();
            });
    }

    const handleAuthenticationAttemptedLegacy = (error) => {
        console.log('handleAuthenticationAttemptedLegacy', error)
    };


    return (
        <View >
            <View >

                <Image

                    source={require('./assets/finger_print.png')}
                />

                <Text>
                    Biometric{'\n'}Authentication
          </Text>


                <TouchableOpacity
                    onPress={() => console.log('nikita')}
                >
                    <Text >
                        BACK TO MAIN
            </Text>
                </TouchableOpacity>

            </View>
        </View>
    )
}

export default NewLoginScreen;

const styles = StyleSheet.create({
    container: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center'
    }
});