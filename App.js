import React from "react";
import { 
  View,
  Text,
  StyleSheet
} from "react-native";

import LoginScreen from "./src/LoginScreen";

const App = (props) => (
  <View style={styles.container}>
    <LoginScreen/>
  </View>
  )
export default App;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center'
  }
});